package com.zero2oneit.mall.goods.controller;

import com.zero2oneit.mall.common.bean.goods.GoodsProductInfo;
import com.zero2oneit.mall.common.bean.goods.Products;
import com.zero2oneit.mall.common.query.goods.GoodsProductInfoQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.goods.feign.SearchFeign;
import com.zero2oneit.mall.goods.service.GoodsProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Description: 商品管理（社区）
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-02-22
 */
@RestController
@RequestMapping("/admin/communityGoods")
public class GoodsProductInfoController {

    @Autowired
    private GoodsProductInfoService goodsProductService;

    @Autowired
    private SearchFeign searchFeign;

    /**
     * 根据不同状态获取对应的列表信息
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody GoodsProductInfoQueryObject qo) {
        return goodsProductService.pageList(qo);
    }

    /**
     * 添加或编辑商品分类（社区团购）信息
     * @param goods
     * @return
     */
    @PostMapping("/addOrEdit")
    public R addOrEdit(@RequestBody GoodsProductInfo goods){
        return goodsProductService.addOrEdit(goods);
    }

    /**
     * 更改商品（社区）信息状态
     * @param id
     * @param status
     * @return
     */
    @PostMapping("/status")
    public R status(String id, Integer status){
        goodsProductService.status(id, status);
        return R.ok();
    }

    /**
     * 更改商品（社区）信息状态
     * @param ids
     * @param status
     * @return
     */
    @PostMapping("/down")
    public R down(String ids, Integer status){
        goodsProductService.down(ids, status);
        for (String id : ids.split(",")) {
            searchFeign.delete(id);
        }
        return R.ok("商品下架成功");
    }

    /**
     * 商品审核（社区）信息状态
     * @param ids
     * @param productStatus
     * @param approveResult
     * @return
     */
    @PostMapping("/audit")
    public R audit(String ids, Integer productStatus, String approveResult, String userName){
        goodsProductService.audit(ids, productStatus, approveResult, userName);
        if(productStatus == 1){
            //添加或编辑ES中的通用商品
            List<Products> products = goodsProductService.esList(ids);
            if(products.size() > 0){
                searchFeign.addOrEdit(products);
            }
        }
        return R.ok();
    }

    /**
     * 绑定商品
     * @param productIds
     * @param ruleId
     * @return
     */
    @PostMapping("/bind")
    public R bind(String productIds, String ruleId, String moudleId){
        goodsProductService.bind(productIds, ruleId, moudleId);
        return R.ok();
    }

    /**
     * 解绑商品
     * @param productIds
     * @return
     */
    @PostMapping("/unBind")
    public R unBind(String productIds){
        goodsProductService.unBind(productIds);
        return R.ok();
    }

    /**
     * 加载某个商品（社区）信息
     * @param id
     * @return
     */
    @PostMapping("/load")
    public R load(String id){
        return R.ok(goodsProductService.load(id));
    }

    /**
     * 复制某个商品（社区）信息
     * @param id
     * @return
     */
    @PostMapping("/copy")
    public R copy(String id){
        for (String s : id.split(",")) {
           goodsProductService.copy(s);
        }
        return R.ok("复制商品成功");
    }

}
