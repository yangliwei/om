package com.zero2oneit.mall.service.member.remote;

import com.zero2oneit.mall.common.annotion.OperateLog;
import com.zero2oneit.mall.common.bean.member.PrizeInfo;
import com.zero2oneit.mall.common.enums.BusinessType;
import com.zero2oneit.mall.common.query.member.PrizeInfoQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.feign.member.PrizeFeign;
import com.zero2oneit.mall.feign.oss.OssFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Description:
 *
 * @author Cmx
 * @date 2021/5/24 14:54
 */
@RestController
@RequestMapping("/remote/prizeInfo")
@CrossOrigin
public class PrizeInfoRemote {

    @Autowired
    private PrizeFeign ruleFeign;

    @Autowired
    private OssFeign ossFeign;

    /**
     * 查询中奖奖品列表信息
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody PrizeInfoQueryObject qo){
        return ruleFeign.prizeInfoList(qo);
    }

    /**
     * 添加或编辑中奖奖品信息
     * @param prizeInfo
     * @return
     */
    @OperateLog(title = "添加或编辑中奖奖品信息", businessType = BusinessType.UPDATE)
    @PostMapping("/addOrEdit")
    public R addOrEdit(PrizeInfo prizeInfo, @RequestParam("file") MultipartFile[] file){
        String imgUrl = null;
        if (file != null && file.length >= 1) {
            imgUrl = ossFeign.uploadImage(file, "advert");
            if(prizeInfo.getId() == null){
                prizeInfo.setLogo(imgUrl);
            }else {
                prizeInfo.setLogo(imgUrl + (prizeInfo.getLogo().length() > 0 ? ","+prizeInfo.getLogo() : ""));
            }
        }

        return ruleFeign.addOrEdit(prizeInfo);
    }
}
