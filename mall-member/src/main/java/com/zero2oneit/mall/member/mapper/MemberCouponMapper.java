package com.zero2oneit.mall.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zero2oneit.mall.common.bean.member.MemberCoupon;
import com.zero2oneit.mall.common.query.member.MemberCouponQueryObject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * @author Tg
 * @create 2021-05-23
 * @description
 */
@Mapper
public interface MemberCouponMapper extends BaseMapper<MemberCoupon> {

    int selectTotal(MemberCouponQueryObject qo);

    List<HashMap<String,Object>> selectRows(MemberCouponQueryObject qo);

    List<HashMap<String, Object>> load(MemberCouponQueryObject qo);

    void status(@Param("id") Long id, @Param("status") Integer status);

}
