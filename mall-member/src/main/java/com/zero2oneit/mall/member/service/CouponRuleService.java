package com.zero2oneit.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zero2oneit.mall.common.bean.member.CouponRule;
import com.zero2oneit.mall.common.query.member.CouponRuleQueryObject;
import com.zero2oneit.mall.common.query.member.MemberCouponQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-03-19
 */
public interface CouponRuleService extends IService<CouponRule> {

    BoostrapDataGrid pageList(CouponRuleQueryObject qo);

    R status(String id, Integer status);

    void reduce(MemberCouponQueryObject qo);

    R copy(String id);

}

